<?php

namespace jf\Tex;

use jf\Tex\Macro\Macro;

/**
 * Renderiza el texto con el color especificado.
 */
class TextColor extends Macro
{
    /**
     * Construye una instancia a partir del nombre y el contenido.
     *
     * @param string $name    Nombre que se pasará como primer argumento.
     * @param string $content Contenido de la macro que se pasará como segundo argumento.
     *
     * @return static
     */
    public static function fromNameAndContent(string $name, string $content) : static
    {
        return static::fromArguments([ $name, $content ]);
    }
}
