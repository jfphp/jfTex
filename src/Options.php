<?php

namespace jf\Tex;

/**
 * Representa una macro que forma parte de una sección o que puede insertarse directamente en el documento.
 */
class Options extends Latex
{
    use TWrap;

    /**
     * @inheritdoc
     */
    public function __construct(array $values = [])
    {
        $this->brackets();
        parent::__construct($values);
    }

    /**
     * @inheritdoc
     */
    public function renderItems(Document $document, array $items, ?string $glue = NULL, bool $indent = FALSE) : string
    {
        if ($items)
        {
            foreach ($items as $option => $value)
            {
                if ($value instanceof ILatex)
                {
                    $value = $value->build($document);
                }
                elseif (is_array($value))
                {
                    $value = static::fromItems($value)->braces()->build($document);
                }
                elseif (is_string($option))
                {
                    if (is_bool($value))
                    {
                        $value = $value ? 'true' : 'false';
                    }
                }
                $items[ $option ] = is_numeric($option)
                    ? $value
                    : "$option=$value";
            }
            $options = $this->wrap(implode($glue ?? ',', $items));
            if ($this->counter)
            {
                $options = '[' . count($items) . ']' . $options;
            }
        }
        else
        {
            $options = '';
        }

        return $options;
    }
}
