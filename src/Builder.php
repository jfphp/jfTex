<?php

namespace jf\Tex;

use jf\Base\TDasherize;

/**
 * Clase para construir diversos elementos.
 */
class Builder extends ABase
{
    use TDasherize;

    /**
     * Devuelve el listado de valores opcionales especificados en una macro o sección.
     *
     * @param array|string $arguments Argumentos a analizar.
     *
     * @return string[]
     */
    public static function arguments(array|string $arguments) : array
    {
        $args = [];
        if ($arguments)
        {
            if (!is_array($arguments))
            {
                $arguments = [ $arguments ];
            }
            foreach ($arguments as $argument)
            {
                if (preg_match_all('/#(\d+)/', $argument, $matches))
                {
                    $args = [...$args, ...$matches[0]];
                }
            }
            if ($args)
            {
                $args = array_unique($args);
                sort($args);
            }
        }

        return $args;
    }

    /**
     * Convierte un texto en un comentario para que pueda ser insertado en el documento.
     *
     * @param object|string $text  Texto a mostrar en el comentario.
     * @param int           $width Ancho del comentario.
     *
     * @return string
     */
    public static function comment(object|string $text, int $width = 120) : string
    {
        if (is_object($text))
        {
            $text = get_class($text);
        }
        $line = str_repeat('%', $width);

        return sprintf("%s\n%% %s %%\n%s", $line, str_pad($text, $width - 4), $line);
    }

    /**
     * Escapa aquellos caracteres que tienen significado especial en TeX.
     *
     * @param string $text Texto a modificar.
     *
     * @return string
     */
    public static function escape(string $text) : string
    {
        return strtr(
            $text,
            [
                "\u{00A0}" => '~',
                '\\'       => '\\\\',
                '{'        => '\{',
                '}'        => '\}',
                '"'        => '``',
                '_'        => '\_',
                '#'        => '\#',
                '$'        => '\$',
                '%'        => '\%',
                '&'        => '\&',
            ]
        );
    }

    /**
     * Construye el identificador apropiado eliminando los caracteres no permitidos.
     *
     * @param string $text Texto a modificar.
     *
     * @return string
     */
    public static function identifier(string $text) : string
    {
        return preg_replace('/[^a-zA-Z@]+/', '', strtolower($text));
    }

    /**
     * Indenta el texto agregando espacios al inicio.
     *
     * @param string|string[] $text Texto a indentar.
     *
     * @return string
     */
    public static function indent(array|string $text) : string
    {
        return preg_replace('/^/m', '    ', trim(is_array($text) ? implode(PHP_EOL, $text) : $text));
    }

    /**
     * Extrae el último segmento de un texto unido por separadores.
     *
     * @param string $text Texto a analizar.
     * @param string $sep  Separador usado entre las partes del texto.
     *
     * @return string
     */
    public static function last(string $text, string $sep = '\\') : string
    {
        $pos = strrpos($text, $sep);

        return $pos === FALSE ? $text : substr($text, $pos + 1);
    }

    /**
     * Construye el identificador apropiado eliminando los caracteres no permitidos.
     *
     * @param string $name Texto a modificar.
     *
     * @return string
     */
    public static function placeholder(string $name) : string
    {
        return preg_replace('/[^\w\-.+]+/', '', $name);
    }
}
