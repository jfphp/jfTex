<?php

namespace jf\Tex;

use jf\Tex\List\Description;
use jf\Tex\List\Enumerate;
use jf\Tex\List\Itemize;
use jf\Tex\Macro\Vspace;

/**
 * Construye una página usando los valores almacenados en las propiedades.
 * Las propiedades serán ordenadas para la correcta colocación en el documento.
 * La primera letra del nombre de la propiedad debe ser la especificada en `autosep`, por defecto una `s`.
 *
 * Como ejemplo, si quisiéramos una sección con 2 parráfos seguida de otra sección con un párrafo,
 * definiríamos las siguientes propiedades:
 *
 * ```php
 * class MyPage extends APage
 * {
 *     use TAutoPage;
 *
 *     public $s1    = 'Sección 1';
 *     public $s1s1p = 'Sección 1. Párrafo 1';
 *     public $s1s2p = 'Sección 1. Párrafo 2';
 *
 *     public $s2    = 'Sección 2';
 *     public $s2s1p = 'Sección 2. Párrafo 1';
 * }
 * ```
 *
 * El texto final del nombre (despúes del último dígito) se usa para saber cuál macro usar.
 * Ver el método `_addItemsFromProperty` para determinar los valores soportados.
 */
trait TAutoPage
{
    /**
     * Separador a usar para llenar automáticamente la página a partir de las propiedades.
     *
     * @var string
     */
    public string $autosep = 's';

    /**
     * @see Page::_items
     */
    protected array $_items = [];

    /**
     * @see Page::addItems()
     */
    abstract public function addItems(IItems|array|string|NULL $items) : static;

    /**
     * Agrega los elementos de la página a partir de los valores almacenados en las propiedades de la clase.
     *
     * @param array $properties Listado de propiedades que serán agregadas.
     *
     * @return static
     */
    protected function _addItemsFromProperties(array $properties) : static
    {
        foreach ($properties as $items)
        {
            ksort($items, SORT_STRING);
            foreach ($items as $key => $item)
            {
                $this->_addItemsFromProperty(
                    $key !== '' && preg_match('/^\d+(\w+)$/', $key, $matches)
                        ? $matches[1]
                        : $key,
                    $item,
                    $items
                );
            }
        }

        return $this;
    }

    /**
     * Agrega el elemento a la clase según el nombre de la propiedad que se está procesando.
     *
     * @param string $name       Nombre de la propiedad siendo procesada.
     * @param mixed  $value      Valor de la propiedad.
     * @param array  $properties Nombre de todas las propiedades de la misma sección.
     *
     * @return void
     */
    protected function _addItemsFromProperty(string $name, mixed $value, array $properties) : void
    {
        match ($name)
        {
            ''   => $this->section($value),
            'c'  => $this->chapter($value),
            'd'  => $this->addItems(Description::fromItems(is_array($value) ? $value : [ $value ])),
            'e'  => $this->addItems(Enumerate::fromItems(is_array($value)   ? $value : [ $value ])),
            'i'  => $this->addItems(Itemize::fromItems(is_array($value)     ? $value : [ $value ])),
            's'  => $this->subsection($value),
            'ss' => $this->subsubsection($value),
            'p'  => $this->paragraph($value),
            'v'  => $this->addItems(Vspace::fromSize($value))
        };
    }

    /**
     * Hook que se ejecuta después de empezar el proceso de agregado automático.
     *
     * @param Document $document Documento siendo procesado.
     *
     * @return static
     */
    protected function _afterAddItemsFromProperties(Document $document) : static
    {
        return $this;
    }

    /**
     * Hook que se ejecuta antes de empezar el proceso de agregado automático.
     *
     * @param Document $document Documento siendo procesado.
     *
     * @return static
     */
    protected function _beforeAddItemsFromProperties(Document $document) : static
    {
        return $this;
    }

    /**
     * @see Page::build()
     */
    public function build(Document $document) : ILatex|array|string|NULL
    {
        if (!$this->_items)
        {
            $this
                ->_startPage()
                ->_beforeAddItemsFromProperties($document)
                ->_addItemsFromProperties($this->_getItemsProperties())
                ->_afterAddItemsFromProperties($document);
        }

        return parent::build($document);
    }

    /**
     * @see Page::chapter()
     */
    abstract public function chapter(ILatex|string $text, bool $starred = TRUE) : static;

    /**
     * Devuelve el listado de propiedades que serán agregadas como elementos del documento.
     *
     * @return array
     */
    protected function _getItemsProperties() : array
    {
        $sep  = $this->autosep;
        $list = [];
        foreach ($this as $prop => $value)
        {
            if ($value !== NULL && preg_match("/^$sep(\d+)(.*)$/", $prop, $matches))
            {
                $name = $matches[2];
                if (empty($name) || $name[0] === $sep)
                {
                    $list[ $matches[1] ][ substr($name, 1) ] = $value;
                }
            }
        }
        ksort($list, SORT_NUMERIC);

        return $list;
    }

    /**
     * @see Page::paragraph()
     */
    abstract public function paragraph(ILatex|string $text, bool $starred = TRUE) : static;

    /**
     * @see Page::section()
     */
    abstract public function section(ILatex|string $text, bool $starred = TRUE) : static;

    /**
     * @see Page::_startPage()
     */
    abstract protected function _startPage() : static;

    /**
     * @see Page::subsection()
     */
    abstract public function subsection(ILatex|string $text, bool $starred = TRUE) : static;

    /**
     * @see Page::subsubsection()
     */
    abstract public function subsubsection(ILatex|string $text, bool $starred = TRUE) : static;
}
