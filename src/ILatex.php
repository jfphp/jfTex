<?php

namespace jf\Tex;

/**
 * Interfaz que deben cumplir los elementos que generan código LaTeX que será agregado al documento.
 */
interface ILatex extends IItems
{
    /**
     * Construye el código latex y agrega al documento cualquier macro que deba ir en el preámbulo.
     *
     * @param Document $document Documento siendo procesado.
     *
     * @return ILatex|array|string|NULL
     */
    public function build(Document $document) : ILatex|array|string|NULL;

    /**
     * Prefijo usado en los valores de reemplazo.
     *
     * @return string
     */
    public function prefix() : string;
}
