<?php

namespace jf\Tex\Package;

use jf\Tex\Builder;
use jf\Tex\Document;
use jf\Tex\ILatex;
use jf\Tex\Latex;
use jf\Tex\Macro\UsePackage;

/**
 * Gestiona los metadatos PDF del documento.
 */
class PdfInfo extends Latex
{
    /**
     * Autor del documento.
     *
     * @var string
     */
    public string $author = '';

    /**
     * Creador del documento.
     *
     * @var string
     */
    public string $creator = '';

    /**
     * Opciones de configuración del paquete `hyperref`.
     *
     * @var array
     */
    public array $hyperref = [];

    /**
     * @inheritdoc
     */
    protected array $_items = [ <<<TEX
        \pdfinfo {
            /CreationDate (D:<<pdf_info.date>>)
            /ModDate (D:<<pdf_info.date>>)
            /Author (<<pdf_info.author>>)
            /Creator (<<pdf_info.creator>>)
            /Producer (<<pdf_info.producer>>)
            /Subject (<<pdf_info.subject>>)
            /Title (<<pdf_info.title>>)
        }
        \hypersetup{
            pdfauthor={<<pdf_info.author>>},
            pdfcreator={<<pdf_info.creator>>},
            pdfkeywords={<<pdf_info.keywords>>},
            pdfsubject={<<pdf_info.subject>>}
        }
        TEX
     ];

    /**
     * Palabras claves del documento.
     *
     * @var string
     */
    public string $keywords = '';

    /**
     * Productor del documento.
     *
     * @var string
     */
    public string $producer = '';

    /**
     * Asunto del documento.
     *
     * @var string
     */
    public string $subject = '';

    /**
     * Título del documento.
     *
     * @var string
     */
    public string $title = '';

    /**
     * @inheritdoc
     */
    public function build(Document $document) : ILatex|array|string|NULL
    {
        $author       = $this->author;
        $prefix       = $this->prefix();
        $placeholders = [
            "$prefix.date"     => date('YmdHis'),
            "$prefix.author"   => $author,
            "$prefix.creator"  => $this->creator ?: $author,
            "$prefix.keywords" => $this->keywords,
            "$prefix.producer" => $this->producer ?: $author,
            "$prefix.subject"  => $this->subject,
            "$prefix.title"    => $this->title
        ];
        // Si se han asignado al documento los valores de reemplazo los eliminamos para no sobrescribirlos.
        foreach ($placeholders as $key => $placeholder)
        {
            if ($document->getPlaceholder($key) !== NULL)
            {
                unset($placeholders[ $key ]);
            }
        }
        if ($placeholders)
        {
            $document->addPlaceholders($placeholders);
        }
        $document->addPreamble([
            Builder::comment(static::class),
            UsePackage::fromArgumentsAndOptions('hyperref', $this->hyperref),
            parent::build($document)
        ]);

        return NULL;
    }
}
