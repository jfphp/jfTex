<?php

namespace jf\Tex\Package;

use jf\Tex\Builder;
use jf\Tex\Document;
use jf\Tex\ILatex;
use jf\Tex\Latex;

/**
 * Configura las secciones de la página usando el paquete `sectsty`.
 */
class FancyHeader extends Latex
{
    /**
     * Configuración del pie de página.
     *
     * @var array
     *
     * @var array{L:string,R:string}
     */
    protected array $_footers = [
        'L' => 'Joaquín Fernández',
        'R' => '\thepage'
    ];

    /**
     * Grosor de la línea del pie de página.
     *
     * @var float
     */
    protected float $_frule = 0.0;

    /**
     * Configuración del encabezado.
     *
     * @var array
     *
     * @var array{L:string,R:string}
     */
    protected array $_headers = [
        'L' => '\leftmark',
        'R' => 'Título'
    ];

    /**
     * Altura del encabezado.
     *
     * @var float
     */
    protected float $_height = 15.0;

    /**
     * Grosor de la línea del encabezado.
     *
     * @var float
     */
    protected float $_hrule = 0.0;

    /**
     * Estilo de la página.
     *
     * @var string
     */
    protected string $_style = 'fancy';

    /**
     * Agrega la configuración del pie de página.
     *
     * @param array $footers Configuración a agregar.
     *
     * @return static
     */
    public function addFooters(array $footers) : static
    {
        foreach ($footers as $pos => $footer)
        {
            if ($pos === 'L' || $pos === 'R')
            {
                $this->_footers[ $pos ] = "\\fancyfoot[$pos]{$footer}";
            }
        }

        return $this;
    }

    /**
     * Agrega la configuración del pie de página.
     *
     * @param array $headers Configuración a agregar.
     *
     * @return static
     */
    public function addHeaders(array $headers) : static
    {
        foreach ($headers as $pos => $header)
        {
            if ($pos === 'L' || $pos === 'R')
            {
                $this->_footers[ $pos ] = $header;
            }
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function build(Document $document) : NULL
    {
        $preamble = [
            Builder::comment($this),
            '\usepackage{fancyhdr}',
            '\fancyhf{}'
        ];
        if ($this->_style)
        {
            $preamble[] = "\pagestyle{{$this->_style}}";
        }
        $mark = FALSE;
        foreach (['fancyfoot' => $this->_footers, 'fancyhead' => $this->_headers] as $macro => $items)
        {
            foreach ($items as $pos => $value)
            {
                if (preg_match('#\\\\(left|right)mark#', $value))
                {
                    $mark = TRUE;
                }
                if ($value instanceof ILatex)
                {
                    $value = $value->build($document);
                }
                $preamble[] = "\\{$macro}[$pos]{{$value}}";
            }
        }
        $preamble[] = "\\renewcommand{\\footrulewidth}{{$this->_frule}pt}";
        $preamble[] = "\\renewcommand{\\headrulewidth}{{$this->_hrule}pt}";
        if ($mark)
        {
            $preamble[] = '\renewcommand{\chaptermark}[1]{\markboth{#1}{}}';
            $preamble[] = '\renewcommand{\sectionmark}[1]{\markboth{#1}{}}';
        }
        $preamble[] = "\\setlength{\\headheight}{{$this->_height}pt}";
        $document->addPreamble($preamble);

        return NULL;
    }
}
