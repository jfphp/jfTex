<?php

namespace jf\Tex\Package;

use jf\Tex\Builder;
use jf\Tex\Document;
use jf\Tex\Latex;
use jf\Tex\Macro\UsePackage;

/**
 * Configura las secciones de la página usando el paquete `sectsty`.
 */
class Sectsty extends Latex
{
    /**
     * Configuración del capítulo.
     *
     * @var string
     */
    public string $chapter = '';

    /**
     * Profundidad del contador de las secciones.
     *
     * @var int|NULL
     */
    public ?int $depth;

    /**
     * Configuración del párrafo.
     *
     * @var string
     */
    public string $paragraph = '';

    /**
     * Configuración de la parte de un capítulo.
     *
     * @var string
     */
    public string $part = '';

    /**
     * Configuración de la sección.
     *
     * @var string
     */
    public string $section = '';

    /**
     * Configuración de la subsección.
     *
     * @var string
     */
    public string $subsection = '';

    /**
     * Configuración de la subsubsección.
     *
     * @var string
     */
    public string $subsubsection = '';

    /**
     * @inheritdoc
     */
    public function build(Document $document) : NULL
    {
        $preamble = $this->_preamble($document);
        if ($preamble)
        {
            $document->addPreamble([
                Builder::comment(static::class),
                UsePackage::fromName('sectsty'),
                '\makeatletter',
                ...$preamble,
                '\makeatother'
            ]);
        }

        return NULL;
    }

    /**
     * Construye los elementos que se agregarán al preámbulo del documento.
     *
     * @param Document $document Documento siendo procesado.
     *
     * @return string[]
     *
     * @noinspection PhpUnusedParameterInspection
     */
    protected function _preamble(Document $document) : array
    {
        $preamble = [];
        foreach ($this as $prop => $value)
        {
            if ($prop[0] !== '_')
            {
                if ($value && is_string($value))
                {
                    $preamble[] = "\\renewcommand\\$prop{{$value}}";
                }
                elseif (is_int($value))
                {
                    $preamble[] = "\\setcounter{secnumdepth}{{$value}}";
                }
            }
        }

        return $preamble;
    }
}
