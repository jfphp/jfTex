<?php

namespace jf\Tex\Package;

use jf\Tex\Macro\UsePackage;

/**
 * Configura las dimensiones de la página usando el paquete `geometry`.
 */
class Geometry extends UsePackage
{
    /**
     * Margen inferior.
     *
     * @var string
     */
    public string $bottom = '3cm';

    /**
     * Altura del encabezado.
     *
     * @var string
     */
    public string $headHeight = '1cm';

    /**
     * Separación del encabezado.
     *
     * @var string
     */
    public string $headSep = '1cm';

    /**
     * Indica si se debe redondear el alto del texto al entero superior.
     *
     * @var bool
     */
    public bool $heightRounded = TRUE;

    /**
     * Margen izquierdo.
     *
     * @var string
     */
    public string $left = '2cm';

    /**
     * Margen derecho.
     *
     * @var string
     */
    public string $right = '2cm';

    /**
     * Margen superior.
     *
     * @var string
     */
    public string $top = '3cm';
}
