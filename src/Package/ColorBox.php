<?php

namespace jf\Tex\Package;

use jf\Tex\Builder;
use jf\Tex\Document;
use jf\Tex\Latex;
use jf\Tex\Macro\Macro;
use jf\Tex\Macro\UsePackage;
use jf\Tex\Options;

/**
 * Gestiona los cuadros de texto coloreados.
 */
class ColorBox extends Latex
{
    /**
     * Configuración de cuadros de textos predefinidos.
     *
     * @var Macro[]
     */
    private array $_boxes = [];

    /**
     * Opciones del paquete.
     *
     * @var string[]
     */
    public array $options = [ 'skins' ];

    /**
     * Agrega un listado de cuadros de color.
     *
     * @param array  $boxes Listado de cuadros de color a agregar.
     * @param string $macro Nombre de la macro a usar para definir el cuadro de color.
     *
     * @return static
     *
     * @assign
     */
    public function addBoxes(array $boxes, string $macro = 'newtcolorbox') : static
    {
        foreach ($boxes as $name => $box)
        {
            $code = Macro::fromName("$macro{{$name}}", $box);
            $args = $code->getArguments();
            if ($args instanceof Options)
            {
                $args->close = '}';
                $args->open  = '{';
            }
            $opts = $code->getOptions();
            if ($opts instanceof Options)
            {
                $opts->counter = TRUE;
            }
            $this->_boxes[ $name ] = $code;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function build(Document $document) : NULL
    {
        $preamble = $this->_boxes;
        if ($preamble)
        {
            array_unshift($preamble, UsePackage::fromArgumentsAndOptions('tcolorbox', $this->options));
            $document->addPreamble([ Builder::comment(static::class), $preamble ]);
        }

        return NULL;
    }
}
