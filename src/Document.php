<?php

namespace jf\Tex;

use jf\Base\String\TPlaceholders;
use jf\Tex\Environment\Environment;
use jf\Tex\Macro\DocumentClass;
use jf\Tex\Macro\UsePackage;

/**
 * Gestiona un archivo con formato `TeX`.
 */
class Document extends ABase
{
    use TPlaceholders
    {
        TPlaceholders::addPlaceholders as private _addPlaceholders;
    }

    /**
     * Configuración del documento a usar.
     *
     * @var DocumentClass|NULL
     */
    public ?DocumentClass $class = NULL;

    /**
     * Elementos del documento.
     *
     * @var ILatex[]
     */
    private array $_items = [];

    /**
     * Listado de paquetes a incluir.
     *
     * @var array
     */
    private array $_packages = [
        'babel'    => [ 'spanish' ],
        'fontenc'  => [ 'T1' ],
        'inputenc' => [ 'utf8' ]
    ];

    /**
     * Elementos del preámbulo del documento.
     *
     * @var array<ILatex|string>
     */
    private array $_preamble = [];

    /**
     * @inheritdoc
     */
    public function __construct(array $values = [])
    {
        $this->openPlaceholder  = '<<';
        $this->closePlaceholder = '>>';
        parent::__construct($values);
    }

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return $this->build();
    }

    /**
     * Agrega un elemento al documento.
     *
     * @param ILatex|array|string $item Elemento a agregar.
     *
     * @return static
     */
    public function addItem(ILatex|array|string $item) : static
    {
        if (is_array($item))
        {
            $this->addItems($item);
        }
        else
        {
            $this->_items[] = $item;
        }

        return $this;
    }

    /**
     * Agrega los elementos al documento.
     *
     * @param array<ILatex|string> $items Elementos a agregar.
     *
     * @return static
     */
    public function addItems(array $items) : static
    {
        foreach ($items as $value)
        {
            $this->addItem($value);
        }

        return $this;
    }

    /**
     * Agrega los paquetes al documento.
     *
     * @param ILatex|array|string $packages Listado de paquetes a agregar.
     *
     * @return static
     */
    public function addPackages(ILatex|array|string $packages) : static
    {
        if ($packages)
        {
            if (!is_array($packages))
            {
                $packages = [ $packages ];
            }
            foreach ($packages as $package => $options)
            {
                if ($options === NULL)
                {
                    unset($this->_packages[ $package ]);
                }
                elseif (is_int($package))
                {
                    if (is_string($options))
                    {
                        $this->_packages[ $options ] = [];
                    }
                    elseif ($options instanceof UsePackage)
                    {
                        $this->_packages[ $options->package() ] = $options;
                    }
                    else
                    {
                        $this->_packages[] = $options;
                    }
                }
                elseif (isset($this->_packages[ $package ]))
                {
                    if (is_array($options))
                    {
                        foreach ($options as $option => $value)
                        {
                            $this->_packages[ $package ][ $option ] = $value;
                        }
                    }
                    else
                    {
                        $this->_packages[ $package ][] = $options;
                    }
                }
                else
                {
                    $this->_packages[ $package ] = is_array($options) ? $options : [ $options ];
                }
            }
        }

        return $this;
    }

    /**
     * @see TPlaceholders::addPlaceholders()
     */
    public function addPlaceholders(array $placeholders, string $prefix = '', bool $escape = FALSE) : static
    {
        if ($escape)
        {
            foreach ($placeholders as $key => $value)
            {
                if ($value && is_string($value))
                {
                    $placeholders[ $key ] = Builder::escape($value);
                }
            }
        }

        return $this->_addPlaceholders($placeholders, $prefix, $escape);
    }

    /**
     * Agrega los elementos al preámbulo del documento.
     *
     * @param ILatex|array|string $items Listado de elementos a agregar.
     *
     * @return static
     */
    public function addPreamble(ILatex|array|string $items) : static
    {
        if (is_array($items))
        {
            foreach ($items as $item)
            {
                $this->addPreamble($item);
            }
        }
        elseif ($items)
        {
            $this->_preamble[] = $items;
        }

        return $this;
    }

    /**
     * Construye el código LaTeX del documento.
     *
     * @return string
     */
    public function build() : string
    {
        $block    = Environment::fromItems(
            $this->_items ?: [ Builder::comment('Documento vacío') ],
            [ 'name' => 'document', 'indent' => FALSE ]
        )
            ->build($this);
        $preamble = [ ($this->class ?? new DocumentClass())->build($this) ];
        foreach ($this->_packages as $package => $options)
        {
            if (!$options instanceof UsePackage)
            {
                $options = UsePackage::fromNameAndOptions($package, $options);
            }
            $tex              = $options->build($this);
            $preamble[ $tex ] = $tex;
        }
        foreach ($this->_preamble as $item)
        {
            $tex              = $this->_parseItem($item);
            $preamble[ $tex ] = $tex;
        }

        return static::render(
            implode(
                PHP_EOL,
                [
                    ...array_values($preamble),
                    Builder::comment('Documento'),
                    $block
                ]
            ),
            $this->getPlaceholders()
        );
    }

    /**
     * Analiza el elemento y lo modifica si es necesario.
     *
     * @param ILatex|array|string $item Elemento a analizar.
     *
     * @return string
     */
    protected function _parseItem(ILatex|array|string $item) : string
    {
        if ($item instanceof ILatex)
        {
            $item = $item->build($this);
            if ($item instanceof ILatex)
            {
                $item = $this->_parseItem($item);
            }
            elseif (is_array($item))
            {
                $item = $this->_parseItem($item);
            }
        }
        elseif (is_array($item))
        {
            $items = [];
            foreach ($item as $content)
            {
                $content = $this->_parseItem($content);
                if ($content !== NULL)
                {
                    $items[] = $content;
                }
            }
            $item = implode(PHP_EOL, $items);
        }

        return $item;
    }

    /**
     * Asigna la fuente del documento.
     *
     * @param string $font Nombre de la fuente a usar.
     *
     * @return void
     */
    public function setFont(string $font) : void
    {
        $this->_preamble['familydefault'] = '\renewcommand{\familydefault}{\sfdefault}';
        $this->_preamble['sfdefault']     = "\\renewcommand{\\sfdefault}{{$font}}";
    }
}
