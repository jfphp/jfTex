<?php

namespace jf\Tex;

/**
 * Elemento que implementa la interfaz `ILatex`.
 */
class Latex extends ABase implements ILatex
{
    /**
     * Texto a usar para unir los elementos luego de renderizarlos.
     *
     * @var string
     */
    protected string $_glue = "\n\n";

    /**
     * Contenido del elemento.
     *
     * @var array<ILatex|string>
     */
    protected array $_items = [];

    /**
     * Prefijo de la sección.
     *
     * @var string
     */
    protected string $_prefix = '';

    /**
     * @inheritdoc
     */
    public function addItems(IItems|array|string|NULL $items) : static
    {
        if ($items !== NULL)
        {
            if (is_array($items))
            {
                foreach ($items as $key => $item)
                {
                    if (is_numeric($key))
                    {
                        $this->_items[] = $item;
                    }
                    else
                    {
                        $this->_items[ $key ] = $item;
                    }
                }
            }
            else
            {
                $this->_items[] = $items;
            }
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function build(Document $document) : ILatex|array|string|NULL
    {
        return $this->renderItems($document, $this->getItems());
    }

    /**
     * @inheritdoc
     */
    public static function fromItems(IItems|array|string $items, array $values = []) : static
    {
        $values['items'] = is_array($items) ? $items : [ $items ];

        return static::fromArray($values);
    }

    /**
     * @inheritdoc
     */
    public function getItems() : array
    {
        return $this->_items;
    }

    /**
     * @inheritdoc
     */
    public function prefix() : string
    {
        return $this->_prefix ?: Builder::dasherize(Builder::last(static::class), '_');
    }

    /**
     * @inheritdoc
     */
    public function renderItems(Document $document, array $items, ?string $glue = NULL, bool $indent = FALSE) : string
    {
        $rendered = [];
        foreach ($items as $item)
        {
            if ($item instanceof ILatex)
            {
                $item = $item->build($document);
                if (is_array($item))
                {
                    $item = $this->renderItems($document, $item, $glue);
                }
            }
            if ($item)
            {
                $rendered[] = $item;
            }
        }
        $rendered = implode($glue ?? $this->_glue, $rendered);

        return $indent
            ? Builder::indent($rendered)
            : $rendered;
    }
}
