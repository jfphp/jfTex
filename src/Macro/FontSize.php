<?php

namespace jf\Tex\Macro;

/**
 * Asigna el tamaño de la fuenta.
 */
class FontSize extends Macro
{
    /**
     * @inheritdoc
     */
    public function getItems() : array
    {
        if (!$this->_items)
        {
            $this->addItems(Macro::fromName('selectfont'));
        }

        return parent::getItems();
    }
}
