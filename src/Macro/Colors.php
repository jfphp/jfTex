<?php

namespace jf\Tex\Macro;

use jf\Tex\Builder;
use jf\Tex\Document;
use jf\Tex\Latex;

/**
 * Gestiona los colores de un documento LaTeX.
 */
class Colors extends Latex
{
    /**
     * Listado de colores a agregar al preámbulo.
     * La clave es el nombre y su valor es un listado RGB con cantidades entre 0 y 255.
     *
     * @var array<string,string[]>
     */
    public array $colors = [];

    /**
     * Nombre del paquete que gestiona los colores.
     *
     * @var string
     */
    public string $package = 'xcolor';

    /**
     * Agrega un color al documento.
     *
     * @param string             $name  Nombre del color.
     * @param array|float|string $color Valor del color.
     *                                  Puede ser un texto RGB en formato `#RGB` o `'#RRGGBB`,
     *                                  puede ser un listado de 3 valores menores a 1 como `[0.858, 0.188, 0.478]`,
     *                                  puede ser un listado de 3 enteros positivos menores a 256 `[237,248,248]`
     *                                  y también se acepta un valor menor a 1 para una escala de grises.
     *
     * @return static
     */
    public function addColor(string $name, array|float|string $color) : static
    {
        if ($name && $color)
        {
            if (is_string($color))
            {
                if ($color[0] === '#')
                {
                    $color = substr($color, 1);
                }
                // RGB
                if (strlen($color) === 3)
                {
                    $color = $color[0] . $color[0] . $color[1] . $color[1] . $color[2] . $color[2];
                }
                $decimal = hexdec($color);

                $this->colors[ $name ] = [
                    'RGB',
                    [
                        ($decimal & 0xFF0000) >> 16,
                        ($decimal & 0x00FF00) >> 8,
                        $decimal & 0x0000FF
                    ]
                ];
            }
            elseif (is_float($color))
            {
                $this->colors[ $name ] = ['gray', $color];
            }
            elseif (count($color) === 3)
            {
                if (is_float(current($color)))
                {
                    $this->colors[ $name ] = ['rgb', $color];
                }
                else
                {
                    $this->colors[ $name ] = ['RGB', $color];
                }
            }
        }

        return $this;
    }

    /**
     * Agrega un grupo de colores.
     *
     * @param array $groups Grupo de colores a agregar.
     *
     * @return void
     */
    public function addColorGroups(array $groups) : void
    {
        foreach ($groups as $group => $names)
        {
            if (is_array($names))
            {
                foreach ($names as $name => $color)
                {
                    $this->addColor("$group$name", $color ?: '000');
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function build(Document $document) : NULL
    {
        $colors = $this->colors;
        if ($colors)
        {
            foreach ($colors as $color => [ $type, $values ])
            {
                $colors[ $color ] = Macro::fromName(
                    'definecolor',
                    ['arguments' => [ Builder::identifier($color), $type, implode(',', $values)] ]
                );
            }
            if ($this->package)
            {
                $document->addPackages($this->package);
            }
            array_unshift($colors, Builder::comment(static::class));
            $document->addPreamble([ $colors, ...$this->getItems() ]);
            if (isset($color['textprimary']))
            {
                $document->addPreamble('\color{textprimary}');
            }
        }

        return NULL;
    }
}
