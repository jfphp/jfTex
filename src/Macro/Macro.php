<?php

namespace jf\Tex\Macro;

use jf\Tex\Arguments;
use jf\Tex\Builder;
use jf\Tex\Document;
use jf\Tex\IItems;
use jf\Tex\ILatex;
use jf\Tex\Latex;
use jf\Tex\Options;

/**
 * Representa una macro que forma parte de una sección o que puede insertarse directamente en el documento.
 */
class Macro extends Latex
{
    /**
     * Argumentos de la macro.
     *
     * @var ILatex|NULL
     */
    protected ?ILatex $_arguments = NULL;

    /**
     * Nombre de la macro.
     *
     * @var string
     */
    protected string $_name = '';

    /**
     * Opciones de la macro.
     *
     * @var ILatex|NULL
     */
    protected ?ILatex $_options = NULL;

    /**
     * Algunas macros pueden usar un `*` después del nombre.
     *
     * @var bool
     */
    protected bool $_starred = FALSE;

    /**
     * Agrega los argumentos de la macro.
     *
     * @param ILatex|array|string|NULL $arguments Argumentos a agregar.
     *
     * @return void
     *
     * @assign
     */
    public function addArguments(ILatex|array|string|NULL $arguments) : void
    {
        if ($arguments)
        {
            if ($this->_arguments)
            {
                $this->_arguments->addItems($arguments);
            }
            else
            {
                $this->_arguments = $arguments instanceof ILatex
                    ? $arguments
                    : Arguments::fromItems($arguments);
            }
        }
    }

    /**
     * Agrega las opciones de la macro.
     *
     * @param ILatex|array|string|NULL $options Opciones a agregar.
     *
     * @return void
     *
     * @assign
     */
    public function addOptions(ILatex|array|string|NULL $options) : void
    {
        if ($options)
        {
            if ($this->_options)
            {
                $this->_options->addItems($options);
            }
            else
            {
                $this->_options = $options instanceof ILatex
                    ? $options
                    : Options::fromItems($options);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function build(Document $document) : ILatex|array|string|NULL
    {
        if (!$this->_arguments)
        {
            $this->addArguments($this->_initArguments());
        }
        if (!$this->_options)
        {
            $this->addOptions($this->_initOptions());
        }

        return '\\' . $this->_name() . $this->_buildOptions($document) . $this->_buildArguments($document) . parent::build($document);
    }

    /**
     * Construye los argumentos de la macro.
     *
     * @param Document $document Documento siendo procesado.
     *
     * @return string
     */
    protected function _buildArguments(Document $document) : string
    {
        return $this->_arguments?->build($document) ?? '';
    }

    /**
     * Construye el nombre de la opción tal como lo usa la macro.
     *
     * @param string $name Nombre de la opción que será formateado.
     *
     * @return string
     */
    protected function _buildOptionName(string $name) : string
    {
        return Builder::identifier(Builder::dasherize($name));
    }

    /**
     * Construye las opciones de la macro.
     *
     * @param Document $document Documento siendo procesado.
     *
     * @return string
     */
    protected function _buildOptions(Document $document) : string
    {
        return $this->_options?->build($document) ?? '';
    }

    /**
     * Construye una macro a partir de sus argumentos.
     *
     * @param ILatex|array|string $arguments Argumentos de la macro.
     * @param array               $values    Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromArguments(ILatex|array|string $arguments, array $values = []) : static
    {
        $values['arguments'] = $arguments;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de sus argumentos y los elementos que gestionará.
     *
     * @param ILatex|array|string $arguments Argumentos de la macro.
     * @param IItems|array|string $items     Listado de elementos a gestionar por la instancia.
     * @param array               $values    Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromArgumentsAndItems(ILatex|array|string $arguments, IItems|array|string $items, array $values = []) : static
    {
        $values['arguments'] = $arguments;
        $values['items']     = $items;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de su nombre y sus argumentos.
     *
     * @param ILatex|array|string $arguments Argumentos de la macro.
     * @param ILatex|array|string $options   Opciones de la macro.
     * @param array               $values    Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromArgumentsAndOptions(ILatex|array|string $arguments, ILatex|array|string $options, array $values = []) : static
    {
        $values['arguments'] = $arguments;
        $values['options']   = $options;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de su nombre y sus argumentos.
     *
     * @param ILatex|array|string $arguments Argumentos de la macro.
     * @param ILatex|array|string $options   Opciones de la macro.
     * @param IItems|array|string $items     Listado de elementos a gestionar por la instancia.
     * @param array               $values    Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromArgumentsAndOptionsAndItems(ILatex|array|string $arguments, ILatex|array|string $options, IItems|array|string $items, array $values = []) : static
    {
        $values['arguments'] = $arguments;
        $values['items']     = $items;
        $values['options']   = $options;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de su nombre.
     *
     * @param string $name   Nombre de la macro.
     * @param array  $values Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromName(string $name, array $values = []) : static
    {
        $values['name'] = $name;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de su nombre y sus argumentos.
     *
     * @param string              $name      Nombre de la macro.
     * @param ILatex|array|string $arguments Argumentos de la macro.
     * @param array               $values    Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromNameAndArguments(string $name, ILatex|array|string $arguments, array $values = []) : static
    {
        $values['arguments'] = $arguments;
        $values['name']      = $name;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de su nombre, sus argumentos y sus elementos.
     *
     * @param string              $name      Nombre de la macro.
     * @param ILatex|array|string $arguments Argumentos de la macro.
     * @param IItems|array|string $items     Listado de elementos a gestionar por la instancia.
     * @param array               $values    Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromNameAndArgumentsAndItems(string $name, ILatex|array|string $arguments, IItems|array|string $items, array $values = []) : static
    {
        $values['arguments'] = $arguments;
        $values['items']     = $items;
        $values['name']      = $name;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de su nombre, sus argumentos y sus opciones.
     *
     * @param string              $name      Nombre de la macro.
     * @param ILatex|array|string $arguments Argumentos de la macro.
     * @param ILatex|array|string $options   Opciones de la macro.
     * @param array               $values    Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromNameAndArgumentsAndOptions(string $name, ILatex|array|string $arguments, ILatex|array|string $options, array $values = []) : static
    {
        $values['arguments'] = $arguments;
        $values['name']      = $name;
        $values['options']   = $options;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de su nombre y sus opciones.
     *
     * @param string              $name    Nombre de la macro.
     * @param ILatex|array|string $options Opciones de la macro.
     * @param array               $values  Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromNameAndOptions(string $name, ILatex|array|string $options, array $values = []) : static
    {
        $values['options'] = $options;
        $values['name']    = $name;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de su nombre, sus opciones y sus elementos.
     *
     * @param string              $name    Nombre de la macro.
     * @param ILatex|array|string $options Opciones de la macro.
     * @param IItems|array|string $items   Listado de elementos a gestionar por la instancia.
     * @param array               $values  Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromNameAndOptionsAndItems(string $name, ILatex|array|string $options, IItems|array|string $items, array $values = []) : static
    {
        $values['items']   = $items;
        $values['options'] = $options;
        $values['name']    = $name;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de sus opciones.
     *
     * @param ILatex|array|string $options Opciones de la macro.
     * @param array               $values  Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromOptions(ILatex|array|string $options, array $values = []) : static
    {
        $values['options'] = $options;

        return static::fromArray($values);
    }

    /**
     * Construye una macro a partir de sus opciones y los elementos que gestionará.
     *
     * @param ILatex|array|string $options Opciones de la macro.
     * @param IItems|array|string $items   Listado de elementos a gestionar por la instancia.
     * @param array               $values  Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromOptionsAndItems(ILatex|array|string $options, IItems|array|string $items, array $values = []) : static
    {
        $values['items']   = $items;
        $values['options'] = $options;

        return static::fromArray($values);
    }

    /**
     * Devuelve los argumentos de la macro.
     *
     * @return ILatex|NULL
     */
    public function getArguments() : ?ILatex
    {
        return $this->_arguments;
    }

    /**
     * Devuelve las opciones de la macro.
     *
     * @return ILatex|NULL
     */
    public function getOptions() : ?ILatex
    {
        return $this->_options;
    }

    /**
     * Inicializa los argumentos del paquete.
     *
     * @return ILatex|string[]
     */
    protected function _initArguments() : ILatex|array
    {
        return [];
    }

    /**
     * Inicializa las opciones del paquete.
     *
     * @return ILatex|string[]
     */
    protected function _initOptions() : ILatex|array
    {
        $options = [];
        foreach ($this as $prop => $value)
        {
            if ($prop[0] !== '_')
            {
                if ($value === TRUE)
                {
                    $options[] = $this->_buildOptionName($prop);
                }
                elseif ($value !== NULL)
                {
                    $options[ $this->_buildOptionName($prop) ] = $value;
                }
            }
        }

        return $options;
    }

    /**
     * Devuelve el nombre de la macro.
     *
     * @return string
     */
    protected function _name() : string
    {
        $name = $this->_name;
        if (!$name)
        {
            $this->_name = $name = Builder::identifier(Builder::dasherize(Builder::last(static::class)));
        }
        if ($this->_starred)
        {
            $name .= '*';
        }

        return $name;
    }
}
