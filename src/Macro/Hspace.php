<?php

namespace jf\Tex\Macro;

/**
 * Deja un espacio horizontal.
 */
class Hspace extends Macro
{
    /**
     * @inheritdoc
     */
    protected bool $_starred = TRUE;

    /**
     * Devuelve una instancia a partir del tamaño del espacio.
     *
     * @param string $size Tamaño del espacio a dejar.
     *
     * @return static
     */
    public static function fromSize(string $size) : static
    {
        return static::fromArguments($size);
    }
}
