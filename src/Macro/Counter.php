<?php

namespace jf\Tex\Macro;

use jf\Tex\Builder;
use jf\Tex\Document;

/**
 * Define un contador.
 *
 * ```
 * $doc->addItem(Counter::fromName('dummy'));                   // \newcounter{dummy}
 * $doc->addItem(Counter::fromNameAndArguments('dummy', '1'));  // \newcounter{dummy}\setcounter{dummy}{1}
 * ```
 */
class Counter extends Macro
{
    /**
     * @inheritdoc
     */
    public function build(Document $document) : NULL
    {
        $name = $this->_name();
        $document->addPreamble(Macro::fromNameAndArguments('newcounter', Builder::identifier($name)));
        $args = $this->_arguments;
        if ($args)
        {
            $args->addItems($name);
            $document->addPreamble(Macro::fromNameAndArguments('setcounter', $args));
        }

        return NULL;
    }
}
