<?php

namespace jf\Tex\Macro;

/**
 * Tamaño de fuente (normal + 5).
 */
class Huge2 extends Macro
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'Huge';
}
