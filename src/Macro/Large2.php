<?php

namespace jf\Tex\Macro;

/**
 * Tamaño de fuente (normal + 2).
 */
class Large2 extends Macro
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'Large';
}
