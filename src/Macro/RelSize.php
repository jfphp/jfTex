<?php

namespace jf\Tex\Macro;

use jf\Tex\Document;
use jf\Tex\ILatex;

/**
 * Aumenta o disminuye el tamaño de la fuente.
 */
class RelSize extends Macro
{
    /**
     * @inheritdoc
     */
    protected bool $_starred = FALSE;

    /**
     * @inheritdoc
     */
    public function build(Document $document) : ILatex|array|string|NULL
    {
        $document->addPackages('relsize');

        return parent::build($document);
    }

    /**
     * Devuelve una instancia a partir del tamaño del espacio.
     *
     * @param string $size Tamaño del espacio a dejar.
     *
     * @return static
     */
    public static function fromSize(string $size) : static
    {
        return static::fromArguments($size);
    }
}
