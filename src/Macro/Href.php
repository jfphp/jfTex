<?php

namespace jf\Tex\Macro;

use jf\Tex\ILatex;

class Href extends Macro
{
    /**
     * URL del elemento.
     *
     * @var ILatex|string
     */
    protected ILatex|string $_url = '';

    /**
     * Crea una instancia a partir de la URL y el título.
     *
     * @param ILatex|string       $url   URL del elemento.
     * @param ILatex|array|string $title Título a mostrar en vez del enlace.
     *
     * @return static
     */
    public static function fromUrlAndTitle(ILatex|string $url, ILatex|array|string $title) : static
    {
        return static::fromArray([ 'items' => $title ?: $url, 'url' => $url ]);
    }

    /**
     * @inheritdoc
     */
    public function getItems() : array
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    protected function _initArguments() : ILatex|array
    {
        return [ $this->_url, $this->_items ];
    }
}
