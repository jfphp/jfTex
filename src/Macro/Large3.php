<?php

namespace jf\Tex\Macro;

/**
 * Tamaño de fuente (normal + 3).
 */
class Large3 extends Macro
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'LARGE';
}
