<?php

namespace jf\Tex\Macro;

use jf\Tex\Builder;
use jf\Tex\Document;

/**
 * Define una macro.
 */
class Def extends Macro
{
    /**
     * Número de argumentos que acepta la macro.
     *
     * @var int|NULL
     */
    protected ?int $_numArgs = NULL;

    /**
     * @inheritdoc
     */
    protected function _buildArguments(Document $document) : string
    {
        $args = parent::_buildArguments($document);

        return $args && !$this->_numArgs
            ? implode('', Builder::arguments($args)) . $args
            : $args;
    }

    /**
     * @inheritdoc
     */
    protected function _name() : string
    {
        return 'def\\' . Builder::identifier(parent::_name());
    }
}
