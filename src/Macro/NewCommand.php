<?php

namespace jf\Tex\Macro;

/**
 * Crea un comando nuevo.
 */
class NewCommand extends Macro
{
    /**
     * Construye una instancia a partir del nombre y el valor.
     *
     * @param string $name  Nombre de la macro.
     * @param string $value Valor a asignar.
     *
     * @return static
     */
    public static function fromNameAndValue(string $name, string $value) : static
    {
        return static::fromArguments([ "\\$name", $value ]);
    }
}
