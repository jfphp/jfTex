<?php

namespace jf\Tex\Macro;

/**
 * Macro para definir la clase del documento.
 */
class DocumentClass extends Macro
{
    /**
     * Clase de documento.
     *
     * @var string
     */
    public string $class = 'article';

    /**
     * Fuente base del documento.
     *
     * @var string
     */
    public string $fontsize = '11pt';

    /**
     * Tamaño del papel a usar.
     *
     * @var string
     */
    public string $paper = 'a4paper';

    /**
     * @inheritdoc
     */
    protected function _initArguments() : array
    {
        return ['class' => $this->class];
    }

    /**
     * @inheritdoc
     */
    protected function _initOptions() : array
    {
        $options = parent::_initOptions();
        unset($options['class']);

        return [ implode(',', $options) ];
    }
}
