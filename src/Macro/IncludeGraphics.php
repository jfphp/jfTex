<?php

namespace jf\Tex\Macro;

use jf\Tex\ILatex;

/**
 * Gestiona la inclusión de imágenes.
 */
class IncludeGraphics extends Macro
{
    /**
     * Anchura de la imagen.
     *
     * @var float|string|NULL
     */
    public float|string|NULL $height = NULL;

    /**
     * @inheritdoc
     */
    protected string $_name = 'includegraphics';

    /**
     * Ruta de la imagen.
     *
     * @var string
     */
    protected string $_path = '';

    /**
     * Anchura de la imagen.
     *
     * @var float|string|NULL
     */
    public float|string|NULL $width = NULL;

    /**
     * Construye una instancia a partir de la ruta de la imagen, su alto y su ancho.
     *
     * @param string            $path   Ruta de la imagen.
     * @param float|string|NULL $height Altura del icono.
     * @param float|string|NULL $width  Anchura del icono.
     *
     * @return static
     */
    public static function fromPathAndHeightAndWidth(string $path, float|string|NULL $height, float|string|NULL $width) : static
    {
        return parent::fromArray([ 'height' => $height, 'path' => $path, 'width' => $width ]);
    }

    /**
     * @inheritdoc
     */
    protected function _initArguments() : ILatex|array
    {
        return [ $this->_path ];
    }

    /**
     * @inheritdoc
     */
    protected function _initOptions() : ILatex|array
    {
        $options = parent::_initOptions();
        foreach ($options as $option => $value)
        {
            if (is_float($value))
            {
                $options[ $option ] = "{$value}pt";
            }
        }

        return $options;
    }
}
