<?php

namespace jf\Tex\Macro;

use jf\Tex\Document;

/**
 * Importa un paquete.
 */
class UsePackage extends Macro
{
    /**
     * @inheritdoc
     */
    public function build(Document $document) : string
    {
        if (!$this->_arguments)
        {
            $this->addArguments([ $this->package() ]);
        }

        return parent::build($document);
    }

    /**
     * @inheritdoc
     */
    protected function _name() : string
    {
        return 'usepackage';
    }

    /**
     * Devuelve el nombre del paquete a importar.
     *
     * @return string
     */
    public function package() : string
    {
        return parent::_name();
    }
}
