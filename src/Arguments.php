<?php

namespace jf\Tex;

/**
 * Gestiona los argumentos de una macro.
 */
class Arguments extends Latex
{
    use TWrap;

    /**
     * @inheritdoc
     */
    public function renderItems(Document $document, array $items, ?string $glue = NULL, bool $indent = FALSE) : string
    {
        if ($items)
        {
            $close = $this->close;
            $open  = $this->open;
            foreach ($items as $argument => $value)
            {
                if ($value instanceof ILatex)
                {
                    $value = $value->build($document);
                }
                elseif (is_string($argument))
                {
                    if (is_bool($value))
                    {
                        $value = $value ? 'true' : 'false';
                    }
                    elseif (is_array($value))
                    {
                        $value = is_array(current($value))
                            ? implode('', array_map(fn($o) => $this->renderItems($document, $o, $glue, $indent), $value))
                            : implode(',', $value);
                    }
                }
                elseif (is_array($value))
                {
                    $value = $this->renderItems($document, $value, $glue, $indent);
                }
                $items[ $argument ] = $this->wrap($value);
            }
            $items = implode($open ? '' : ($glue ?? ','), $items);
        }

        return $items ?: '';
    }
}
