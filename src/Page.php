<?php

namespace jf\Tex;

use jf\Tex\Environment\Environment;
use jf\Tex\Macro\Macro;

/**
 * Gestiona una página en formato `LaTeX`.
 */
class Page extends Latex
{
    /**
     * Índice del párrafo actual siendo procesado.
     *
     * @var int
     */
    protected int $_paragraph = 0;

    /**
     * Índice de la sección actual siendo procesada.
     *
     * @var int
     */
    protected int $_section = 0;

    /**
     * Título de la página.
     *
     * @var string
     */
    public string $title = '';

    /**
     * Agrega un listado de párrafos a la sección actual.
     *
     * @param int|NULL $count Cantidad de párrafos a agregar.
     *
     * @return static
     */
    protected function _addParagraphs(?int $count = NULL) : static
    {
        $section = $this->_section;
        $sname   = "s{$section}_p";
        if ($count === NULL)
        {
            $count = 0;
            foreach ($this as $prop => $value)
            {
                if (str_starts_with($prop, $sname))
                {
                    ++$count;
                }
            }
        }
        while ($count > 0)
        {
            $index    = ++$this->_paragraph;
            $property = $sname . $index;
            $content  = $this->$property ?? NULL;
            if ($content !== NULL)
            {
                $this->paragraph($content);
            }
            --$count;
        }

        return $this;
    }

    /**
     * Construye el código latex y agrega al documento cualquier macro que deba ir en el preámbulo.
     *
     * @param Document $document Documento siendo procesado.
     *
     * @return ILatex|array|string|NULL
     */
    public function build(Document $document) : ILatex|array|string|NULL
    {
        $placeholders = $this->_placeholders($document);
        if ($placeholders)
        {
            $document->addPlaceholders($placeholders, $this->prefix());
        }

        return parent::build($document);
    }

    /**
     * Agrega a la página un texto haciendo uso de la macro `\chapter{$text}`.
     *
     * @param ILatex|string $text    Texto a agregar.
     * @param bool          $starred Indica si la macro requiere un asterisco en el nombre.
     *
     * @return static
     */
    public function chapter(ILatex|string $text, bool $starred = TRUE) : static
    {
        return $this->_create('chapter', $text, $starred);
    }

    /**
     * Agrega el texto al documento creando la macro con el nombre especificado.
     *
     * @param string        $name    Nombre de la macro a crear.
     * @param ILatex|string $text    Texto de la macro.
     * @param bool          $starred Indica si la macro requiere un asterisco después de su nombre.
     *
     * @return static
     */
    protected function _create(string $name, ILatex|string $text, bool $starred = TRUE) : static
    {
        return $this->addItems(Macro::fromNameAndArguments($name, [ $text ], [ 'starred' => $starred ]));
    }

    /**
     * Construye un entorno con el nombre y los elementos especificados y lo agrega al documento.
     *
     * @param string              $name   Nombre del entorno a agregar.
     * @param ILatex|array|string $items  Elementos a agregar dentro del entorno.
     * @param array               $values Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public function environment(string $name, ILatex|array|string $items, array $values = []) : static
    {
        $values['name'] = $name;

        return $this->addItems(Environment::fromItems($items, $values));
    }

    /**
     * @inheritdoc
     */
    public function getItems() : array
    {
        return [ $this->header(), ...parent::getItems() ];
    }

    /**
     * @return string
     */
    public function header() : string
    {
        return PHP_EOL . Builder::comment(static::class);
    }

    /**
     * Agrega a la página un texto haciendo uso de la macro `\paragraph{$text}`.
     *
     * @param ILatex|string $text    Texto a agregar.
     * @param bool          $starred Indica si la macro requiere un asterisco en el nombre.
     *
     * @return static
     */
    public function paragraph(ILatex|string $text, bool $starred = TRUE) : static
    {
        return $this->_create('paragraph', $text, $starred);
    }

    /**
     * Agrega a la página un texto haciendo uso de la macro `\part{$text}`.
     *
     * @param ILatex|string $text    Texto a agregar.
     * @param bool          $starred Indica si la macro requiere un asterisco en el nombre.
     *
     * @return static
     */
    public function part(ILatex|string $text, bool $starred = TRUE) : static
    {
        return $this->_create('part', $text, $starred);
    }

    /**
     * Construye los valores de reemplazo y los devuelve.
     *
     * @param Document $document Documento siendo procesado.
     *
     * @return array
     */
    protected function _placeholders(Document $document) : array
    {
        $placeholders = [];
        foreach ($this->buildToArrayValues() as $key => $value)
        {
            if (is_string($value))
            {
                $placeholders[$key] = $value;
            }
        }

        return $placeholders;
    }

    /**
     * Agrega a la página un texto haciendo uso de la macro `\section{$text}`.
     *
     * @param ILatex|string $text    Texto a agregar.
     * @param bool          $starred Indica si la macro requiere un asterisco en el nombre.
     *
     * @return static
     */
    public function section(ILatex|string $text, bool $starred = TRUE) : static
    {
        return $this->_create('section', $text, $starred);
    }

    /**
     * Empieza la página.
     *
     * @return static
     */
    protected function _startPage() : static
    {
        return $this->part($this->title);
    }

    /**
     * Empieza una sección nueva.
     *
     * @param bool $paragraph Indica si se agrega antes un párrafo adicional para aumentar la separación.
     *
     * @return static
     */
    protected function _startSection(bool $paragraph = TRUE) : static
    {
        $section = ++$this->_section;
        if ($section > 1 && $paragraph)
        {
            $this->paragraph('');
        }
        $this->section($this->{"s$section"});
        $this->_paragraph = 0;

        return $this;
    }

    /**
     * Agrega a la página un texto haciendo uso de la macro `\subsection{$text}`.
     *
     * @param ILatex|string $text    Texto a agregar.
     * @param bool          $starred Indica si la macro requiere un asterisco en el nombre.
     *
     * @return static
     */
    public function subsection(ILatex|string $text, bool $starred = TRUE) : static
    {
        return $this->_create('subsection', $text, $starred);
    }

    /**
     * Agrega a la página un texto haciendo uso de la macro `\subsubsection{$text}`.
     *
     * @param ILatex|string $text    Texto a agregar.
     * @param bool          $starred Indica si la macro requiere un asterisco en el nombre.
     *
     * @return static
     */
    public function subsubsection(ILatex|string $text, bool $starred = TRUE) : static
    {
        return $this->_create('subsubsection', $text, $starred);
    }
}
