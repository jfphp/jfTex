<?php

namespace jf\Tex;

use jf\Base\AAssign;

/**
 * Clase base para el resto de clases del proyecto.
 */
abstract class ABase extends AAssign
{
}
