<?php

namespace jf\Tex\List;

use jf\Tex\Document;
use jf\Tex\Environment\Environment;
use jf\Tex\ILatex;

/**
 * Genera una lista de elementos.
 */
abstract class AList extends Environment
{
    /**
     * @inheritdoc
     */
    protected string $_glue = PHP_EOL;

    /**
     * Nombre de la macro a usar para renderizar cada elemento de la lista.
     *
     * @var ILatex|string
     */
    protected ILatex|string $_item = '\item';

    /**
     * @inheritdoc
     */
    protected function _buildItems(Document $document, array $items) : array
    {
        if ($items)
        {
            $macro = $this->_item;
            if ($macro instanceof ILatex)
            {
                $macro = $macro->build($document);
            }
            foreach ($items as $key => $item)
            {
                $items[ $key ] = $macro . ' ' . ltrim($item);
            }
        }

        return $items;
    }
}
