<?php

namespace jf\Tex\List;

/**
 * Listado de elementos.
 */
class Itemize extends AList
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'itemize';
}
