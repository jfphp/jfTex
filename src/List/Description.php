<?php

namespace jf\Tex\List;

/**
 * Listado de descripciones.
 */
class Description extends AList
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'description';
}
