<?php

namespace jf\Tex\List;

/**
 * Enumeración de elementos.
 */
class Enumerate extends AList
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'enumerate';
}
