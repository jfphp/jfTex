<?php

namespace jf\Tex\Generator\Config\Plugin;

use jf\Tex\Environment\Environment;

/**
 * Agrega de manera masiva el listado de entornos simples.
 */
class Environments extends Macros
{
    /**
     * @inheritdoc
     */
    public string $extends = Environment::class;
}