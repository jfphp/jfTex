<?php

namespace jf\Tex\Generator\Config\Plugin;

use jf\Generator\Config\Item\ClassObject;
use jf\Generator\Config\Plugin\APlugin;
use jf\Reflector\ReflectionClass;
use jf\Tex\Macro\Macro;

/**
 * Agrega de manera masiva el listado de macros simples.
 */
class Macros extends APlugin
{
    /**
     * Clase de la cual extienden las macros.
     *
     * @var string
     */
    public string $extends = Macro::class;

    /**
     * @inheritdoc
     */
    protected function _parse(mixed $plugin, array &$global) : array
    {
        $fqcns = [];
        if ($plugin)
        {
            $extends  = $this->extends;
            $defaults = [
                'extends'   => $extends,
                'namespace' => substr($this->getClass(), 0, -1)
            ];
            $prop     = ReflectionClass::fromClassname($extends)->getProperty('_name')->getConfig()->toArray();
            foreach ($plugin as $name => $desc)
            {
                $config = [ 'description' => $desc ];
                if (ctype_upper($name))
                {
                    $config['properties']['_name'] = [
                        ...$prop,
                        'override' => TRUE,
                        'value'    => $name
                    ];

                    $name = self::pascalize(strtolower($name)) . '3';
                }
                elseif (ctype_upper($name[0]) && isset($plugin[ strtolower($name) ]))
                {
                    $config['properties']['_name'] = [
                        ...$prop,
                        'override' => TRUE,
                        'value'    => $name
                    ];

                    $name = self::pascalize(strtolower($name)) . '2';
                }
                else
                {
                    $name = self::pascalize($name);
                }
                $this->_initItem($name, $config, $defaults);
                $item    = ClassObject::fromArray($config);
                $fqcns[] = $item->register($global);
            }
        }

        return $fqcns;
    }
}