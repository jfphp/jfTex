<?php

namespace jf\Tex\Generator\Config\Plugin;

use jf\Generator\Config\Item\ClassObject;
use jf\Generator\Config\Plugin\APlugin;

/**
 * Agrega de manera masiva el listado de entornos para listas simples.
 */
class Lists extends APlugin
{
    /**
     * Clase de la cual extienden las listas.
     *
     * @var string
     */
    public string $extends = 'jf\Tex\List\AList';

    /**
     * @inheritdoc
     */
    protected function _parse(mixed $plugin, array &$global) : array
    {
        $fqcns = [];
        if ($plugin)
        {
            $defaults = [
                'extends'   => [ $this->extends ],
                'namespace' => 'List'
            ];
            $prop     = $global['classes']['jf\Tex\Macro\Macro']->properties['_name'];
            foreach ($plugin as $name => $desc)
            {
                $name                          = self::pascalize($name);
                $config                        = [ 'description' => $desc ];
                $config['properties']['_name'] = [
                    ...$prop,
                    'override' => TRUE,
                    'value'    => strtolower($name)
                ];
                $this->_initItem($name, $config, $defaults);
                $item    = ClassObject::fromArray($config);
                $fqcns[] = $item->register($global);
            }
        }

        return $fqcns;
    }
}