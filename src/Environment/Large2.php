<?php

namespace jf\Tex\Environment;

/**
 * Tamaño de fuente (normal + 2).
 */
class Large2 extends Environment
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'Large';
}
