<?php

namespace jf\Tex\Environment;

use jf\Tex\Document;
use jf\Tex\Macro\Macro;
use jf\Tex\Macro\UsePackage;

/**
 * Genera un gráfico usando el paquete `pgfplots`.
 */
class PgfPlots extends Environment
{
    /**
     * Bibliotas adicionales a importar.
     *
     * @var string[]
     */
    protected array $_libraries = [];

    /**
     * @inheritdoc
     */
    protected string $_name = 'pgfplots';

    /**
     * @inheritdoc
     */
    public function build(Document $document) : string
    {
        $document->addPackages(UsePackage::fromName('pgfplots'));
        $document->addPreamble(Macro::fromNameAndArguments('pgfplotsset', 'compat=newest'));
        if ($this->_libraries)
        {
            $document->addPreamble(Macro::fromNameAndArguments('usepgfplotslibrary', implode(',', $this->_libraries)));
        }

        return parent::build($document);
    }
}
