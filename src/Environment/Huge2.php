<?php

namespace jf\Tex\Environment;

/**
 * Tamaño de fuente (normal + 5).
 */
class Huge2 extends Environment
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'Huge';
}
