<?php

namespace jf\Tex\Environment;

use jf\Tex\Document;

/**
 * Bloque de macros.
 */
class Tabular extends Environment
{
    /**
     * @inheritdoc
     */
    protected string $_body = " \\\\\n";

    /**
     * Configuración de las columnas.
     *
     * @var array
     */
    protected array $_columns = [];

    /**
     * @inheritdoc
     */
    protected string $_name = 'tabular';

    /**
     * Filas de la tabla.
     *
     * @var string[]
     */
    protected array $_rows = [];

    /**
     * @inheritdoc
     */
    public function build(Document $document) : string
    {
        foreach ($this->_rows as $index => $columns)
        {
            $this->addItems(
                is_array($columns)
                    ? $this->renderItems($document, $columns, ' & ')
                    : $columns
            );
            unset($this->_rows[ $index ]);
        }
        $this->addArguments(implode('', $this->_columns));
        if ($this->_name === 'tabularx')
        {
            $document->addPackages('tabularx');
        }

        return parent::build($document);
    }

    /**
     * Asigna la configuración de las columnas.
     *
     * @param array $columns Configuración de las columnas a asignar.
     *
     * @return void
     */
    public function setColumns(array $columns) : void
    {
        foreach ($columns as $column)
        {
            if (is_array($column))
            {
                $left   = $column[0] ?? NULL;
                $right  = $column[2] ?? NULL;
                $column = $column[1] ?? '';
            }
            else
            {
                $left  = NULL;
                $right = NULL;
            }
            $col = $left === NULL
                ? ''
                : "@{{$left}}";
            if (is_numeric($column))
            {
                $column = "m{{$column}cm}";
            }
            $col .= $column;
            if ($right !== NULL)
            {
                $col .= "@{{$right}}";
            }
            $this->_columns[] = $col;
        }
    }
}
