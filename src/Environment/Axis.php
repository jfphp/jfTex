<?php

namespace jf\Tex\Environment;

use jf\Tex\Builder;
use jf\Tex\Document;
use jf\Tex\ILatex;
use jf\Tex\Macro\Macro;
use jf\Tex\Options;

/**
 * Genera un gráfico usando el paquete `tikz`.
 */
class Axis extends Environment
{
    /**
     * @var string|NULL
     */
    public ?string $axisLines = NULL;

    /**
     * @var array|string|NULL
     */
    public array|string|NULL $axisLineStyle = NULL;

    /**
     * @var string|NULL
     */
    public ?string $barShift = NULL;

    /**
     * @var string|NULL
     */
    public ?string $barWidth = NULL;

    /**
     * @var string|NULL
     */
    public ?string $height = NULL;

    /**
     * @var string|NULL
     */
    public ?string $minimumHeight = NULL;

    /**
     * @inheritdoc
     */
    protected string $_name = 'axis';

    /**
     * @var bool|NULL
     */
    public ?bool $scaledTicks = NULL;

    /**
     * @var array|string|NULL
     */
    public array|string|NULL $tickStyle = NULL;

    /**
     * @var string|NULL
     */
    public ?string $width = NULL;

    /**
     * @var float|string|NULL
     */
    public float|string|NULL $xmax = NULL;

    /**
     * @var float|string|NULL
     */
    public float|string|NULL $xmin = NULL;

    /**
     * @var array|NULL
     */
    public ?array $xtick = NULL;

    /**
     * @var array|NULL
     */
    public ?array $xticklabels = NULL;

    /**
     * @var bool|NULL
     */
    public ?bool $ybarStacked = NULL;

    /**
     * @var float|string|NULL
     */
    public float|string|NULL $ymax = NULL;

    /**
     * @var float|string|NULL
     */
    public float|string|NULL $ymin = NULL;

    /**
     * @var array|NULL
     */
    public ?array $ytick = NULL;

    /**
     * @var array|NULL
     */
    public ?array $yticklabels = NULL;

    /**
     * Agrega las coordenadas de un gráfico.
     *
     * @param array|string $coords  Listado de coordenadas.
     * @param array|string $options Opciones del gráfico que se generará.
     *
     * @return static
     */
    public function addCoordinates(array|string $coords, array|string $options = []) : static
    {
        $this->addItems(
            Macro::fromNameAndOptions(
                'addplot',
                $options,
                [
                    'glue'  => ' ',
                    'items' => [
                        ' coordinates {',
                        is_string($coords) ? $coords : implode(' ', $coords),
                        '};'
                    ]
                ]
            )
        );

        return $this;
    }

    /**
     * Agrega un gráfico sombreado.
     *
     * @param array|string $options     Opción del gráfico.
     * @param array|string $fillOptions Opciones del sombreado.
     *
     * @return static
     */
    public function addFillBetween(array|string $options = [], array|string $fillOptions = []) : static
    {
        $this->addItems(
            Macro::fromNameAndOptions(
                'addplot',
                $options,
                [
                    'glue'  => '',
                    'items' => [
                        ' fill between',
                        Options::fromItems($fillOptions),
                        ';'
                    ]
                ]
            )
        );

        return $this;
    }

    /**
     * Agrega el gráfico dentro de un entorno `scope`.
     *
     * @param ILatex|array|string $items   Elementos a agregar dentro del entorno.
     * @param array|string        $options Opciones del entorno.
     *
     * @return static
     */
    public function addScope(ILatex|array|string $items, array|string $options = []) : static
    {
        $this->addItems(Environment::fromNameAndOptions('scope', $options, ['items' => $items]));

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function build(Document $document) : string
    {
        $document->addPackages('pgfplots');

        return parent::build($document);
    }

    /**
     * Construye los valores para mostrar las etiquetas del eje Y.
     *
     * @param float         $max     Valor máximo del grupo de valores.
     * @param float|NULL    $ymax    Valor máximo a usar.
     * @param string[]|NULL $ylabels Listado de etiquetas a mostrar en el eje Y.
     * @param float[]|NULL  $yticks  Listado de los puntos donde se colocarán las etiquetas.
     *
     * @return void
     */
    public function buildLabelsY(float $max, ?float &$ymax, ?array &$ylabels, ?array &$yticks) : void
    {
        $ymax    = $max;
        $ylabels = [];
        $yticks  = [];
        $yinc    = pow(10, floor(log10($max)));
        if ($max / $yinc < 2)
        {
            $yinc /= 2;
        }
        $i   = $yinc;
        $max += $yinc;
        while ($i < $max)
        {
            $ylabel    = $this->_formatLabelY($i);
            $ylabels[] = $ylabel;
            $yticks[]  = $i;
            $i         += $yinc;
        }
        if ($i > $ymax)
        {
            $ymax = $i; // Redondeamos por arriba el valor máximo de Y
        }
    }

    /**
     * @inheritdoc
     */
    protected function _buildOptionName(string $name) : string
    {
        return preg_replace('/[^\w ]+/', '', Builder::dasherize($name, ' '));
    }

    /**
     * Convierte el listado de coordenadas al formato usado por el paquete.
     *
     * @param float[] $data Listado de coordenadas en formato `[ x0 => y0, ..., xn => yn ]`.
     *
     * @return array
     */
    public static function dataToCoordinates(array $data) : array
    {
        $coords = [];
        foreach ($data as $x => $y)
        {
            $coords[] = "($x,$y)";
        }

        return $coords;
    }

    /**
     * Formatea el valor de la etiqueta del eje Y.
     *
     * @param float $value Valor a formatear.
     *
     * @return string
     */
    protected function _formatLabelY(float $value) : string
    {
        return number_format($value, 2, ',', '.');
    }
}
