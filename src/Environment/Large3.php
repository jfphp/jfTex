<?php

namespace jf\Tex\Environment;

/**
 * Tamaño de fuente (normal + 3).
 */
class Large3 extends Environment
{
    /**
     * @inheritdoc
     */
    protected string $_name = 'LARGE';
}
