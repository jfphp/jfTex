<?php

namespace jf\Tex\Environment;

use jf\Tex\Document;
use jf\Tex\Macro\Macro;
use jf\Tex\Macro\UsePackage;

/**
 * Genera un gráfico usando el paquete `tikz`.
 */
class TikzPicture extends Environment
{
    /**
     * Bibliotas adicionales a importar.
     *
     * @var string[]
     */
    protected array $_libraries = [];

    /**
     * @inheritdoc
     */
    protected string $_name = 'tikzpicture';

    /**
     * Agrega un nodo al gráfico.
     *
     * @param string $name   Nombre del nodo.
     * @param array  $coords Coordenadas del nodo.
     * @param string $text   Texto o etiqueta a mostrar en el nodo.
     *
     * @return static
     */
    public function addNode(string $name, array $coords = [ 0, 0 ], string $text = '') : static
    {
        $this->addItems("\\node ($name) at ($coords[0],$coords[1]) {{$text}};");

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function build(Document $document) : string
    {
        $document->addPackages(UsePackage::fromName('tikz'));
        if ($this->_libraries)
        {
            $document->addPreamble(Macro::fromNameAndArguments('usetikzlibrary', implode(',', $this->_libraries)));
        }

        return parent::build($document);
    }
}
