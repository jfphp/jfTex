<?php

namespace jf\Tex\Environment;

use jf\Tex\Document;
use jf\Tex\Macro\Macro;

/**
 * Bloque de macros.
 */
class Environment extends Macro
{
    /**
     * Texto a usar para unir el cuerpo del bloque.
     *
     * @var string
     */
    protected string $_body = PHP_EOL;

    /**
     * @inheritdoc
     */
    protected string $_glue = PHP_EOL;

    /**
     * Indica si se indenta el cuerpo del bloque.
     *
     * @var bool
     */
    protected bool $_indent = TRUE;

    /**
     * @inheritdoc
     */
    public function build(Document $document) : string
    {
        if (!$this->_arguments)
        {
            $this->addArguments($this->_initArguments());
        }
        if (!$this->_options)
        {
            $this->addOptions($this->_initOptions());
        }
        $items = $this->getItems();
        if ($items)
        {
            $name = $this->_name();
            $items = [
                Macro::fromNameAndArguments(
                    "begin{{$name}}",
                    $this->_arguments?->getItems() ?? [],
                    ['options' => $this->_options]
                ),
                $this->renderItems($document, $this->_buildItems($document, $items), $this->_body, $this->_indent),
                Macro::fromName("end{{$name}}")
            ];
        }

        return $items ? $this->renderItems($document, $items) : '';
    }

    /**
     * Construye el cuerpo del bloque.
     *
     * @param Document $document Documento siendo procesado.
     * @param array    $items    Elementos que forman parte del cuerpo del bloque.
     *
     * @return array
     */
    protected function _buildItems(Document $document, array $items) : array
    {
        return $items;
    }
}
