<?php

namespace jf\Tex;

/**
 * Facilita el envolver los textos entre llaves y corchetes.
 */
trait TWrap
{
    /**
     * Carácter usado para cerrar el texto.
     *
     * @var string
     */
    public string $close = '}';

    /**
     * Indica si se agrega el contador al inicio.
     *
     * @var bool
     */
    public bool $counter = FALSE;

    /**
     * Carácter usado para abrir el texto.
     *
     * @var string
     */
    public string $open = '{';

    /**
     * Asigna las llaves como caracteres de apertura y cierre.
     *
     * @return static
     */
    public function braces() : static
    {
        $this->close = '}';
        $this->open  = '{';

        return $this;
    }

    /**
     * Asigna los corchetes como caracteres de apertura y cierre.
     *
     * @return static
     */
    public function brackets() : static
    {
        $this->close = ']';
        $this->open  = '[';

        return $this;
    }

    /**
     * Envuelve el texto especificado entre los caracteres de apertura y cierre.
     *
     * @param string $text Texto a envolver.
     *
     * @return string
     */
    public function wrap(string $text) : string
    {
        return $this->open . $text . $this->close;
    }
}
