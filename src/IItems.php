<?php

namespace jf\Tex;

/**
 * Interfaz para las clases que gestionan elementos.
 */
interface IItems
{
    /**
     * Agrega un listado de elementos.
     *
     * @param IItems|array|string|NULL $items Elementos a agregar.
     *
     * @return static
     */
    public function addItems(IItems|array|string|NULL $items) : static;

    /**
     * Genera una instancia a partir de los elementos que gestionará.
     *
     * @param IItems|array|string $items  Listado de elementos a gestionar por la instancia.
     * @param array               $values Valores adicionales para configurar la instancia.
     *
     * @return static
     */
    public static function fromItems(IItems|array|string $items, array $values = []) : static;

    /**
     * Devuelve el listado de elementos gestionados por la clase.
     *
     * @return array<IItems|string>
     */
    public function getItems() : array;

    /**
     * Construye el contenido a partir del listado de elementos que deben ser colocados en la ubicación especificada.
     *
     * @param Document    $document Documento siendo procesado.
     * @param array       $items    Listado de elementos a construir.
     * @param string|NULL $glue     Texto a usar para unir los elementos.
     * @param bool        $indent   Indica si se debe indentar el contenido renderizado.
     *
     * @return string
     */
    public function renderItems(Document $document, array $items, ?string $glue = NULL, bool $indent = FALSE) : string;
}
