# jf/tex

Clases para simplificar la generación de archivos TeX.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/tex` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/tex
```

#### Dependencias

Cuando el proyecto es instalado, adicionalmente se instalan las siguientes dependencias:

| Paquete | Versión |
|:--------|:--------|
| jf/base | ^4.0.0  |

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone git@gitlab.com:jfphp/jfTex.git
cd jfTex
composer install
```

## Archivos disponibles

### Clases

| Nombre                                                                                      | Descripción                                                                                              |
|:--------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------|
| [jf\Tex\ABase](src/ABase.php)                                                               | Clase base para el resto de clases del proyecto.                                                         |
| [jf\Tex\Arguments](src/Arguments.php)                                                       | Gestiona los argumentos de una macro.                                                                    |
| [jf\Tex\Builder](src/Builder.php)                                                           | Clase para construir diversos elementos.                                                                 |
| [jf\Tex\Document](src/Document.php)                                                         | Gestiona un archivo con formato `TeX`.                                                                   |
| [jf\Tex\Environment\Axis](src/Environment/Axis.php)                                         | Genera un gráfico usando el paquete `tikz`.                                                              |
| [jf\Tex\Environment\Center](src/Environment/Center.php)                                     | Centra el contenido.                                                                                     |
| [jf\Tex\Environment\Environment](src/Environment/Environment.php)                           | Bloque de macros.                                                                                        |
| [jf\Tex\Environment\FlushLeft](src/Environment/FlushLeft.php)                               | Alineación a la izquierda.                                                                               |
| [jf\Tex\Environment\FlushRight](src/Environment/FlushRight.php)                             | Alineación a la derecha.                                                                                 |
| [jf\Tex\Environment\FootnoteSize](src/Environment/FootnoteSize.php)                         | Tamaño de fuente (normal - 2).                                                                           |
| [jf\Tex\Environment\Huge](src/Environment/Huge.php)                                         | Tamaño de fuente (normal + 4).                                                                           |
| [jf\Tex\Environment\Huge2](src/Environment/Huge2.php)                                       | Tamaño de fuente (normal + 5).                                                                           |
| [jf\Tex\Environment\Large](src/Environment/Large.php)                                       | Tamaño de fuente (normal + 1).                                                                           |
| [jf\Tex\Environment\Large2](src/Environment/Large2.php)                                     | Tamaño de fuente (normal + 2).                                                                           |
| [jf\Tex\Environment\Large3](src/Environment/Large3.php)                                     | Tamaño de fuente (normal + 3).                                                                           |
| [jf\Tex\Environment\MiniPage](src/Environment/MiniPage.php)                                 | Páginas de tamaño reducido.                                                                              |
| [jf\Tex\Environment\NormalSize](src/Environment/NormalSize.php)                             | Tamaño de fuente normal.                                                                                 |
| [jf\Tex\Environment\PgfPlots](src/Environment/PgfPlots.php)                                 | Genera un gráfico usando el paquete `pgfplots`.                                                          |
| [jf\Tex\Environment\Quote](src/Environment/Quote.php)                                       | Entorno para citas.                                                                                      |
| [jf\Tex\Environment\RaggedLeft](src/Environment/RaggedLeft.php)                             | Alineación a la derecha.                                                                                 |
| [jf\Tex\Environment\RaggedRight](src/Environment/RaggedRight.php)                           | Alineación a la izquierda.                                                                               |
| [jf\Tex\Environment\Scope](src/Environment/Scope.php)                                       | Ámbito Tikz.                                                                                             |
| [jf\Tex\Environment\ScriptSize](src/Environment/ScriptSize.php)                             | Tamaño de fuente (normal - 3).                                                                           |
| [jf\Tex\Environment\Small](src/Environment/Small.php)                                       | Tamaño de fuente (normal - 1).                                                                           |
| [jf\Tex\Environment\Spacing](src/Environment/Spacing.php)                                   | Cambio de espacio entre líneas.                                                                          |
| [jf\Tex\Environment\Tabular](src/Environment/Tabular.php)                                   | Bloque de macros.                                                                                        |
| [jf\Tex\Environment\TikzPicture](src/Environment/TikzPicture.php)                           | Genera un gráfico usando el paquete `tikz`.                                                              |
| [jf\Tex\Environment\Tiny](src/Environment/Tiny.php)                                         | Tamaño de fuente (normal - 4).                                                                           |
| [jf\Tex\Environment\Verbatim](src/Environment/Verbatim.php)                                 | Entorno para contenido textual.                                                                          |
| [jf\Tex\Generator\Config\Plugin\Environments](src/Generator/Config/Plugin/Environments.php) | Agrega de manera masiva el listado de entornos simples.                                                  |
| [jf\Tex\Generator\Config\Plugin\Lists](src/Generator/Config/Plugin/Lists.php)               | Agrega de manera masiva el listado de entornos para listas simples.                                      |
| [jf\Tex\Generator\Config\Plugin\Macros](src/Generator/Config/Plugin/Macros.php)             | Agrega de manera masiva el listado de macros simples.                                                    |
| [jf\Tex\Latex](src/Latex.php)                                                               | Elemento que implementa la interfaz `ILatex`.                                                            |
| [jf\Tex\List\AList](src/List/AList.php)                                                     | Genera una lista de elementos.                                                                           |
| [jf\Tex\List\Description](src/List/Description.php)                                         | Listado de descripciones.                                                                                |
| [jf\Tex\List\Enumerate](src/List/Enumerate.php)                                             | Enumeración de elementos.                                                                                |
| [jf\Tex\List\Itemize](src/List/Itemize.php)                                                 | Listado de elementos.                                                                                    |
| [jf\Tex\Macro\BfSeries](src/Macro/BfSeries.php)                                             | Fuente gruesa.                                                                                           |
| [jf\Tex\Macro\Chapter](src/Macro/Chapter.php)                                               | Capítulo del documento.                                                                                  |
| [jf\Tex\Macro\Color](src/Macro/Color.php)                                                   | Color a usar para el texto.                                                                              |
| [jf\Tex\Macro\Colors](src/Macro/Colors.php)                                                 | Gestiona los colores de un documento LaTeX.                                                              |
| [jf\Tex\Macro\Counter](src/Macro/Counter.php)                                               | Define un contador.                                                                                      |
| [jf\Tex\Macro\Def](src/Macro/Def.php)                                                       | Define una macro.                                                                                        |
| [jf\Tex\Macro\DocumentClass](src/Macro/DocumentClass.php)                                   | Macro para definir la clase del documento.                                                               |
| [jf\Tex\Macro\Draw](src/Macro/Draw.php)                                                     | Realiza el trazo de un dibujo.                                                                           |
| [jf\Tex\Macro\Fill](src/Macro/Fill.php)                                                     | Relleno horizontal.                                                                                      |
| [jf\Tex\Macro\FlushLeft](src/Macro/FlushLeft.php)                                           | Alineación a la izquierda.                                                                               |
| [jf\Tex\Macro\FlushRight](src/Macro/FlushRight.php)                                         | Alineación a la derecha.                                                                                 |
| [jf\Tex\Macro\FontSize](src/Macro/FontSize.php)                                             | Asigna el tamaño de la fuenta.                                                                           |
| [jf\Tex\Macro\FootnoteSize](src/Macro/FootnoteSize.php)                                     | Tamaño de fuente (normal - 2).                                                                           |
| [jf\Tex\Macro\Hfill](src/Macro/Hfill.php)                                                   | Relleno horizontal.                                                                                      |
| [jf\Tex\Macro\Hline](src/Macro/Hline.php)                                                   | Línea horizontal.                                                                                        |
| [jf\Tex\Macro\Hspace](src/Macro/Hspace.php)                                                 | Deja un espacio horizontal.                                                                              |
| [jf\Tex\Macro\Huge](src/Macro/Huge.php)                                                     | Tamaño de fuente (normal + 4).                                                                           |
| [jf\Tex\Macro\Huge2](src/Macro/Huge2.php)                                                   | Tamaño de fuente (normal + 5).                                                                           |
| [jf\Tex\Macro\IncludeGraphics](src/Macro/IncludeGraphics.php)                               | Gestiona la inclusión de imágenes.                                                                       |
| [jf\Tex\Macro\Item](src/Macro/Item.php)                                                     | Elemento de una lista.                                                                                   |
| [jf\Tex\Macro\Large](src/Macro/Large.php)                                                   | Tamaño de fuente (normal + 1).                                                                           |
| [jf\Tex\Macro\Large2](src/Macro/Large2.php)                                                 | Tamaño de fuente (normal + 2).                                                                           |
| [jf\Tex\Macro\Large3](src/Macro/Large3.php)                                                 | Tamaño de fuente (normal + 3).                                                                           |
| [jf\Tex\Macro\Macro](src/Macro/Macro.php)                                                   | Representa una macro que forma parte de una sección o que puede insertarse directamente en el documento. |
| [jf\Tex\Macro\MdSeries](src/Macro/MdSeries.php)                                             | Fuente mediana.                                                                                          |
| [jf\Tex\Macro\MultiColumn](src/Macro/MultiColumn.php)                                       | Celda que ocupa varias columnas.                                                                         |
| [jf\Tex\Macro\MultiRow](src/Macro/MultiRow.php)                                             | Celda que ocupa varias filas.                                                                            |
| [jf\Tex\Macro\NewCommand](src/Macro/NewCommand.php)                                         | Crea un comando nuevo.                                                                                   |
| [jf\Tex\Macro\NewPage](src/Macro/NewPage.php)                                               | Empieza una página nueva.                                                                                |
| [jf\Tex\Macro\Node](src/Macro/Node.php)                                                     | Nodo Tikz.                                                                                               |
| [jf\Tex\Macro\NormalSize](src/Macro/NormalSize.php)                                         | Tamaño de fuente normal.                                                                                 |
| [jf\Tex\Macro\Paragraph](src/Macro/Paragraph.php)                                           | Párrafo del documento.                                                                                   |
| [jf\Tex\Macro\Part](src/Macro/Part.php)                                                     | Parte del documento.                                                                                     |
| [jf\Tex\Macro\RaggedLeft](src/Macro/RaggedLeft.php)                                         | Alineación a la derecha.                                                                                 |
| [jf\Tex\Macro\RaggedRight](src/Macro/RaggedRight.php)                                       | Alineación a la izquierda.                                                                               |
| [jf\Tex\Macro\RaiseBox](src/Macro/RaiseBox.php)                                             | Eleva el tamaño especificado una caja de texto.                                                          |
| [jf\Tex\Macro\RelSize](src/Macro/RelSize.php)                                               | Aumenta o disminuye el tamaño de la fuente.                                                              |
| [jf\Tex\Macro\RenewCommand](src/Macro/RenewCommand.php)                                     | Renueva un comando ya existente.                                                                         |
| [jf\Tex\Macro\RmFamily](src/Macro/RmFamily.php)                                             | Fuente tipo `Roman`.                                                                                     |
| [jf\Tex\Macro\ScriptSize](src/Macro/ScriptSize.php)                                         | Tamaño de fuente (normal - 3).                                                                           |
| [jf\Tex\Macro\Section](src/Macro/Section.php)                                               | Sección del documento.                                                                                   |
| [jf\Tex\Macro\SelectFont](src/Macro/SelectFont.php)                                         | Selecciona la fuente a usar.                                                                             |
| [jf\Tex\Macro\SetLength](src/Macro/SetLength.php)                                           | Asigna el valor de una longitud.                                                                         |
| [jf\Tex\Macro\SfFamily](src/Macro/SfFamily.php)                                             | Fuente tipo `Sans Serif`.                                                                                |
| [jf\Tex\Macro\ShortStack](src/Macro/ShortStack.php)                                         | Apila verticalmente los elementos.                                                                       |
| [jf\Tex\Macro\Small](src/Macro/Small.php)                                                   | Tamaño de fuente (normal - 1).                                                                           |
| [jf\Tex\Macro\Strut](src/Macro/Strut.php)                                                   | Evita que el elemento ocupe espacio horizontal.                                                          |
| [jf\Tex\Macro\SubSection](src/Macro/SubSection.php)                                         | Subsección del documento.                                                                                |
| [jf\Tex\Macro\SubSubSection](src/Macro/SubSubSection.php)                                   | Subsubsección del documento.                                                                             |
| [jf\Tex\Macro\TextBf](src/Macro/TextBf.php)                                                 | Texto en negritas.                                                                                       |
| [jf\Tex\Macro\TextMd](src/Macro/TextMd.php)                                                 | Texto normal.                                                                                            |
| [jf\Tex\Macro\TextSc](src/Macro/TextSc.php)                                                 | Texto en versalitas.                                                                                     |
| [jf\Tex\Macro\TextSl](src/Macro/TextSl.php)                                                 | Texto inclinado.                                                                                         |
| [jf\Tex\Macro\TextTt](src/Macro/TextTt.php)                                                 | Texto monoespaciado.                                                                                     |
| [jf\Tex\Macro\Tikz](src/Macro/Tikz.php)                                                     | Gráfico usando Tikz.                                                                                     |
| [jf\Tex\Macro\Tiny](src/Macro/Tiny.php)                                                     | Tamaño de fuente (normal - 4).                                                                           |
| [jf\Tex\Macro\TtFamily](src/Macro/TtFamily.php)                                             | Fuente monoespaciada.                                                                                    |
| [jf\Tex\Macro\UsePackage](src/Macro/UsePackage.php)                                         | Importa un paquete.                                                                                      |
| [jf\Tex\Macro\Vfill](src/Macro/Vfill.php)                                                   | Relleno vertical.                                                                                        |
| [jf\Tex\Macro\Vline](src/Macro/Vline.php)                                                   | Línea vertical.                                                                                          |
| [jf\Tex\Macro\Vspace](src/Macro/Vspace.php)                                                 | Deja un espacio vertical.                                                                                |
| [jf\Tex\Options](src/Options.php)                                                           | Representa una macro que forma parte de una sección o que puede insertarse directamente en el documento. |
| [jf\Tex\Package\ColorBox](src/Package/ColorBox.php)                                         | Gestiona los cuadros de texto coloreados.                                                                |
| [jf\Tex\Package\FancyHeader](src/Package/FancyHeader.php)                                   | Configura las secciones de la página usando el paquete `sectsty`.                                        |
| [jf\Tex\Package\Geometry](src/Package/Geometry.php)                                         | Configura las dimensiones de la página usando el paquete `geometry`.                                     |
| [jf\Tex\Package\PdfInfo](src/Package/PdfInfo.php)                                           | Gestiona los metadatos PDF del documento.                                                                |
| [jf\Tex\Package\Sectsty](src/Package/Sectsty.php)                                           | Configura las secciones de la página usando el paquete `sectsty`.                                        |
| [jf\Tex\Page](src/Page.php)                                                                 | Gestiona una página en formato `LaTeX`.                                                                  |
| [jf\Tex\TextColor](src/TextColor.php)                                                       | Renderiza el texto con el color especificado.                                                            |

### Interfaces

| Nombre                          | Descripción                                                                                       |
|:--------------------------------|:--------------------------------------------------------------------------------------------------|
| [jf\Tex\IItems](src/IItems.php) | Interfaz para las clases que gestionan elementos.                                                 |
| [jf\Tex\ILatex](src/ILatex.php) | Interfaz que deben cumplir los elementos que generan código LaTeX que será agregado al documento. |

### Traits

| Nombre                                | Descripción                                                             |
|:--------------------------------------|:------------------------------------------------------------------------|
| [jf\Tex\TAutoPage](src/TAutoPage.php) | Construye una página usando los valores almacenados en las propiedades. |
| [jf\Tex\TWrap](src/TWrap.php)         | Facilita el envolver los textos entre llaves y corchetes.               |

## Scripts

### scripts/generate

Genera el código fuente de todo los archivos del repositorio dentro de directorio `src`.

Ver archivo  [scripts/generate](scripts/generate).
